#!/usr/bin/env bash

# Setup
DJANGOPROJECT=django_project

# Variables
APPENV=local
DBHOST=localhost
DBNAME=$DJANGOPROJECT
DBUSER=django
DBPASSWD=django

echo "Updating packages list"
apt-get -qq update > /dev/null 2>&1
apt-get install python-software-properties -y > /dev/null 2>&1
sudo add-apt-repository ppa:git-core/ppa > /dev/null 2>&1
apt-get -qq update > /dev/null 2>&1

echo "Installing Python Pip"
apt-get install -y python-pip > /dev/null 2>&1

echo "Installing nginx"
apt-get install nginx -y > /dev/null 2>&1

echo "Installing Git"
apt-get install git -y > /dev/null 2>&1

echo "Installing Varnish"
apt-get install apt-transport-https -y > /dev/null 2>&1
apt-get install curl -y > /dev/null 2>&1
curl -s https://repo.varnish-cache.org/ubuntu/GPG-key.txt | apt-key add - > /dev/null 2>&1
echo "deb https://repo.varnish-cache.org/ubuntu/ precise varnish-4.0" >> /etc/apt/sources.list.d/varnish-cache.list > /dev/null 2>&1
apt-get update > /dev/null 2>&1
apt-get install varnish -y > /dev/null 2>&1

echo "Installing MySql/PhpMyAdmin"
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections
apt-get -y install mysql-server-5.5 phpmyadmin > /dev/null 2>&1

echo "Installing Python specific libraries for db and imaging"
apt-get install python-mysqldb -y > /dev/null 2>&1
apt-get build-dep python-imaging -y > /dev/null 2>&1 
apt-get install libjpeg8 libjpeg62-dev libfreetype6 libfreetype6-dev -y > /dev/null 2>&1

echo "Install uWSGI"
apt-get install build-essential python-dev -y > /dev/null 2>&1
apt-get install libxml2-dev > /dev/null 2>&1
apt-get install libxslt1-dev > /dev/null 2>&1
sudo pip install uwsgi > /dev/null 2>&1

echo "Installing Django"
pip install -r /vagrant/requirements.txt > /dev/null 2>&1
pip install --upgrade distribute > /dev/null 2>&1
cd /vagrant
[ -d $DJANGOPROJECT ] && echo '.....project folder already setup' || django-admin.py startproject $DJANGOPROJECT

echo "Setting up environment"
echo ".....MySql"
if ! mysql -uroot -p$DBPASSWD -e 'use $DBNAME' > /dev/null 2>&1; then
    mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME"
    mysql -uroot -p$DBPASSWD -e "grant all privileges on $DBNAME.* to '$DBUSER'@'localhost' identified by '$DBPASSWD'"
fi

echo ".....Nginx"
echo "upstream django { server unix:///tmp/uwsgi.sock; }
server {
    listen 8000;
    server_name localhost;
    charset utf-8;
    sendfile off;
    client_max_body_size 75M;
    location /media  {alias /vagrant/$DJANGOPROJECT/media; }
    location /static {alias /vagrant/$DJANGOPROJECT/static; }
    location / {
        uwsgi_pass django;
        include /vagrant/uwsgi_params;
    }
}
" > /vagrant/nginx_vhost
cp /vagrant/nginx_vhost /etc/nginx/sites-available/nginx_vhost > /dev/null
ln -s /etc/nginx/sites-available/nginx_vhost /etc/nginx/sites-enabled/
rm -rf /etc/nginx/sites-available/default
service nginx restart > /dev/null 2>&1
cd /vagrant/$DJANGOPROJECT
[ -d $DJANGOPROJECT ] && echo '' || echo 'STATIC_ROOT = os.path.join(BASE_DIR, "static/")' >> /vagrant/$DJANGOPROJECT/$DJANGOPROJECT/settings.py
python manage.py collectstatic --noinput > /dev/null 2>&1

echo ".....uWSGI"
cd /vagrant
echo "[uwsgi]
processes=10
socket=/tmp/uwsgi.sock
chmod-socket=666
chdir=/vagrant/$DJANGOPROJECT/
wsgi-file=/vagrant/$DJANGOPROJECT/$DJANGOPROJECT/wsgi.py
module=$DJANGOPROJECT.wsgi:app
master=True
pidfile=/tmp/$DJANGOPROJECT.pid
vacuum=True
harakiri=20
max-requests=5000
uid=www-data
gid=www-data
daemonize=/tmp/$DJANGOPROJECT.log" > uwsgi.ini
sudo uwsgi --ini uwsgi.ini > /dev/null 2>&1

echo ".....Varnish"
sudo service apache2 stop > /dev/null 2>&1
sudo pkill varnishd > /dev/null 2>&1
echo "backend default {
      .host = \"127.0.0.1\";
      .port = \"8000\";
}" > /etc/varnish/default.vcl
sudo varnishd -f /etc/varnish/default.vcl -s malloc,1G -T 127.0.0.1:2000 

echo ".....System"
cp /vagrant/requirements.txt /vagrant/$DJANGOPROJECT/requirements.txt > /dev/null 2>&1
echo 'alias devsvr="python manage.py runserver 0.0.0.0:8002"' >> /home/vagrant/.bashrc
echo "cd /vagrant/$DJANGOPROJECT/" >> /home/vagrant/.bashrc